db.rooms.insertOne(
	{
		name: "single",
		accommodates: 2,
		price: "1,000",
		description: "A simple room with all the basic necessities",
		rooms_available: 10,
		isAvailable: false,
	}
);

db.rooms.insertMany(
	[
		{
			name: "double",
			accommodates: 3,
			price: "2,000",
			description: "A room fit for a small family going on a vacation",
			rooms_available: 5,
			isAvailable: false,
		}
	]
);

db.rooms.find();

db.rooms.updateOne(
		{"name": "double"},
		{
			$set: {
				name: "queen",
				rooms_available: 0,
			}
		}
	);
db.rooms.deleteMany({"rooms_available": 0})